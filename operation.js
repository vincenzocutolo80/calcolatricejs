var equal = false;

function add(value){
    if(equal){
        document.getElementById('expression').value = value;
        equal = false;
    }else if((value != "0" && value != "+" && value != "-" && value != "*" && value != "/") 
            && (document.getElementById('expression').value == "0")){
        document.getElementById('expression').value = value;
    }else{
        document.getElementById('expression').value += value;
    }
    document.getElementById('expression').focus();
}

function equals(){
    document.getElementById('expression').value = eval(document.getElementById('expression').value);
    equal = true;
}

function remove(){
    document.getElementById('expression').value = "";
}